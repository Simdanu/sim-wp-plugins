<?php
/*
Plugin Name: Testimonial
Plugin URI: http://local.sim-wordpress.me/
Description: Simple Testimonial
Version: 1.0
Author: Simon Megadewandanu
Author URI: http://simonndanu.info
*/
function html_form_code() {
    echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post">';
    echo '<p>';
    echo 'Name (required) <br />';
    echo '<input type="text" name="st-name" pattern="[a-zA-Z0-9 ]+" value="' . ( isset( $_POST["st-name"] ) ? esc_attr( $_POST["st-name"] ) : '' ) . '" size="40" />';
    echo '</p>';
    echo '<p>';
    echo 'Email (required) <br />';
    echo '<input type="email" name="st-email" value="' . ( isset( $_POST["st-email"] ) ? esc_attr( $_POST["st-email"] ) : '' ) . '" size="40" />';
    echo '</p>';
    echo '<p>';
    echo 'Phone (required) <br />';
    echo '<input type="text" name="st-phone" pattern="[0-9 ]+" value="' . ( isset( $_POST["st-phone"] ) ? esc_attr( $_POST["st-phone"] ) : '' ) . '" size="40" />';
    echo '</p>';
    echo '<p>';
    echo 'Testimonial (required) <br />';
    echo '<textarea rows="10" cols="35" name="st-testimonial">' . ( isset( $_POST["st-testimonial"] ) ? esc_attr( $_POST["st-testimonial"] ) : '' ) . '</textarea>';
    echo '</p>';
    echo '<p><input type="submit" name="st-submitted" value="Submit"/></p>';
    echo '</form>';
}

add_action('init','testimonial_insert');
function testimonial_insert() {

    // if the submit button is clicked, send the email
    if ( isset( $_POST['st-submitted'] ) ) {
        global $wpdb, $blog_id;
        // sanitize form values
		$name        = sanitize_text_field( $_POST["st-name"] );
		$email       = sanitize_email( $_POST["st-email"] );
		$phone       = sanitize_text_field( $_POST["st-phone"] );
		$testimonial = esc_textarea( $_POST["st-testimonial"] );

		$wpdb->insert( 
			'wpms_testimonials', 
			array( 
				'name'        => $name, 
				'email'       => $email,
				'phone'       => $phone, 
				'testimonial' => $testimonial,
				'blog_id'	  => $blog_id
			), 
			array( 
				'%s', 
				'%s',
				'%s',
				'%s',
				'%d'
			) 
		);
		$_POST = array();
		echo "Testimonial is added succesfully!";
    }
}

add_shortcode( 'testimonial_form', 'st_shortcode' );
function st_shortcode() {
    ob_start();
    html_form_code();
    return ob_get_clean();
}

add_action( 'admin_menu', 'testimonial_admin_menu' );
function testimonial_admin_menu() {
	add_menu_page( 'Testimonial Top Level Menu', 'Testimonial', 'activate_plugins', 'testimonial/testimonial.php', 'testimonial_admin_page', 'dashicons-testimonial', 6  );
}

function testimonial_admin_page(){
	$testimonialTable = new Testimonial_List_Table();
	$testimonialTable->prepare_items();
	echo '<div class="wrap"><h2>Testimonial Table</h2>'; 
	echo '<form id="packages-table" method="GET">';
	echo '<input type="hidden" name="page" value="'.$_REQUEST['page'].'"/>';
	$testimonialTable->display(); 
	echo '</form>';
	echo '</div>'; 
}

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Testimonial_List_Table extends WP_List_Table
{
    function __construct()
    {
        parent::__construct(array());
    }
    
    function column_default($item, $column_name)
    {
        return $item[$column_name];
    }
    
    function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />',
            $item['id']
        );
    }

    function column_name($item) {
	  	$actions = array(
	            'delete' => sprintf('<a href="?page=%s&action=%s&id=%d">Delete</a>',$_REQUEST['page'],'delete',$item['id']),
	        );

	 	return sprintf('%1$s %2$s', $item['name'], $this->row_actions($actions) );
	}

	function column_testimonial($item) {
	  	$actions = array(
	            'delete' => sprintf('<a href="?page=%s&action=%s&id=%d">Delete</a>',$_REQUEST['page'],'delete',$item['id']),
	        );

	 	return sprintf('%1$s %2$s', $item['testimonial'], $this->row_actions($actions) );
	}
    
    function get_columns()
    {
        $columns = array(
			'cb'          => '<input type="checkbox" />',
			'name'        => "Name",
			'email'       => "Email",
			'phone'       => "Phone",
			'testimonial' => "Testimonial"
        );
        return $columns;
    }

    function get_sortable_columns()
    {
        $sortable_columns = array(
			'name'        => array('name', true),
			'email'       => array('email', true),
			'phone'       => array('phone', true),
			'testimonial' => array('testimonial', true)
        );
        return $sortable_columns;
    }


    function get_bulk_actions()
    {
        $actions = array(
            'delete' => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action()
    {
        if ('delete' === $this->current_action()) {
        	global $wpdb, $blog_id;
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids)) $ids = implode(',', $ids);
            if (!empty($ids)) {
                $wpdb->query("DELETE FROM wpms_testimonials WHERE id IN($ids) AND blog_id=".$blog_id);
            }
        }
    }

    function prepare_items()
    {
        global $wpdb, $blog_id;
		$table_name = 'wpms_testimonials';
		$per_page   = 10;
		$columns    = $this->get_columns();
		$hidden     = array();
		$sortable   = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->process_bulk_action();
		$total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name WHERE blog_id=".$blog_id);
		$paged       = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged']) - 1) : 0;
		$orderby     = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'id';
		$order       = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'asc';
        $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE blog_id=".$blog_id." ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged), ARRAY_A);
        $this->set_pagination_args(array(
			'total_items' => $total_items,
			'per_page'    => $per_page,
			'total_pages' => ceil($total_items / $per_page)
        ));
    }
}

add_action( 'widgets_init', 'register_testi_widget' );
function register_testi_widget() {
    register_widget( 'Testi_Widget' );
}

class Testi_Widget extends WP_Widget {

	function __construct() {
		parent::__construct(false, $name = "Testimonial");
	}

	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		global $wpdb, $blog_id;
		$testimonial = $wpdb->get_results($wpdb->prepare("SELECT * FROM wpms_testimonials WHERE blog_id=".$blog_id." ORDER BY RAND() LIMIT 1"), ARRAY_A);
		echo $testimonial[0]['testimonial'].' ('.$testimonial[0]['name'].')';
		echo $args['after_widget'];
	}


	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';

		return $instance;
	}

}



?>