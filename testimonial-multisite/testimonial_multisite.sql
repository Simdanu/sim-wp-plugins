/*
SQLyog Community
MySQL - 5.7.22-0ubuntu0.17.10.1 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `wpms_testimonials` (
	`id` int (10),
	`name` varchar (450),
	`email` varchar (450),
	`phone` varchar (180),
	`testimonial` text ,
	`blog_id` int (10)
); 
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('1','Simon','simon@softwareseni.com','1234567890','qwerzxcv','1');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('2','Sagara','sagara@softwareseni.com','0987654321','asdfghjk','2');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('3','Jony','jony@softwareseni.com','1234567890','poiuytrewq','3');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('4','Anjas','anjas@softwareseni.com','1357908642','mnbvcxzlkjhgfdsa','4');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('5','Simon M','simonm@softwareseni.com','1234567890','mmqwerzxcv','1');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('6','Sagara M','sagaram@softwareseni.com','0987654321','mmasdfghjk','2');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('7','Jony M','jonym@softwareseni.com','1234567890','mmpoiuytrewq','3');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('8','Anjas M','anjasm@softwareseni.com','1357908642','mmmnbvcxzlkjhgfdsa','4');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('11','First Multisite','firstmultisite@softwareseni.com','1234567890','Semoga berkah ya.','2');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('12','Second First','secondfirst@softwareseni.com','777666555','Mencoba bertestimoni lagi','2');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('21','Second Really','secondtapiketiga@softwareseni.com','123789456','Testi Testi','3');
insert into `wpms_testimonials` (`id`, `name`, `email`, `phone`, `testimonial`, `blog_id`) values('22','Pertama yg Utama','wtf@wtf.com','987123465','Uhuy website.','1');
