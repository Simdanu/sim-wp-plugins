<?php
function optionsframework_option_name() {
	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = 'sim_theme_option';
	update_option('optionsframework', $optionsframework_settings);
}

function optionsframework_options() {
	$options = array();

	$options[] = array( "name" => "Second Theme Options", "type" => "heading" );

	$options['logo'] = array(
		"name" => "Logo",
		"desc" => "This creates a full size uploader that previews the image.",
		"id"   => "logo",
		"type" => "upload" );

	$options['display_sidebar'] = array(
		"name" => "Sidebar Option",
		"desc" => "Displayed.",
		"id"   => "display_sidebar",
		"std"  => "1",
		"type" => "checkbox" );

	$options['limit_post'] = array(
		"name"    => "Limit Post",
		"id"      => "limit_post",
		"std"     => "10",
		"type"    => "select",
		"options" => [ "5" => "5", "10" => "10", "20" => "20", "30" => "30" ] );

	return $options;
}
?>