<?php

/* OLD WAY
add_action('admin_menu', 'my_theme_menu');
function my_theme_menu() {
    add_theme_page("Custom Options", "Custom Options", "manage_options", "theme-options", "theme_option_page", null, 99);
}
function theme_option_page() {
?>
    <div class="wrap">
        <h1>Custom Theme Options</h1>
        <form method="post" action="options.php">
            <?php
                settings_fields('theme-options');
                do_settings_sections('theme-options');
                submit_button();
            ?>
        </form>
</div>
<?php
}

add_action('admin_init','theme_settings');
function theme_settings(){
    add_settings_section( 'first_section', 'First Theme Options Section','theme_section_description','theme-options');
    
    add_settings_field("logo", "Logo", "display_logo", "theme-options", "first_section");
    register_setting( 'theme-options', 'logo');

    add_settings_field('footer_copyright', 'Footer Copyright', 'display_footer_copyright', 'theme-options', 'first_section');
    register_setting( 'theme-options', 'footer_copyright');

    add_settings_field('blog_desc', 'Blog Description', 'display_blog_description', 'theme-options', 'first_section');
    register_setting( 'theme-options', 'blog_desc');
}

function theme_section_description(){
    echo '<p>This is the First Theme Option Section</p>';
}

function display_logo()
{
    ?>
    <input type="file" name="logo" />
    <?php  echo get_option('logo');  ?>
    <?php
}

function display_footer_copyright(){
    ?>
    <input type="text" name="footer_copyright" id="footer_copyright" value="<?php echo get_option('footer_copyright'); ?>" />
    <?php
}

function display_blog_description(){
    ?>
    <textarea name="blog_desc" id="blog_desc" rows="4" cols="50"><?php echo get_option('footer_copyright'); ?></textarea>
    <?php
}
*/

if ( !function_exists( 'of_get_option' ) ) {
    function of_get_option($name, $default = false) {

        $optionsframework_settings = get_option('optionsframework');

        // Gets the unique option id
        $option_name = $optionsframework_settings['id'];

        if ( get_option($option_name) ) {
            $options = get_option($option_name);
        }

        if ( isset($options[$name]) ) {
            return $options[$name];
        } else {
            return $default;
        }
    }
}


?>