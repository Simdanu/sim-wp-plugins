<?php
function optionsframework_option_name() {
	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = 'sim_theme_option';
	update_option('optionsframework', $optionsframework_settings);
}

function optionsframework_options() {
	$options = array();

	$options[] = array( "name" => "First Theme Options", "type" => "heading" );

	$options['logo'] = array(
		"name" => "Logo",
		"desc" => "This creates a full size uploader that previews the image.",
		"id"   => "logo",
		"type" => "upload" );
	
	$options['footer_copyright'] = array(
		"name" => "Footer Copyright",
		"id"   => "footer_copyright",
		"type" => "text" );

	$options['description'] = array(
		"name" => "Description",
		"id"   => "description",
		"type" => "textarea" );

	return $options;
}
?>