<?php
/*
Plugin Name: Team Member
Plugin URI: http://local.sim-wordpress.me/
Description: Simple Team Member
Version: 1.0
Author: Simon Megadewandanu
Author URI: http://simonndanu.info
License: simonndanu
*/

add_action( 'init', 'create_team_member' );
function create_team_member() {
    register_post_type( 'team_member',
        array(
            'labels' => array(
				'name'               => 'Team Members',
				'singular_name'      => 'Team Member',
				'add_new'            => 'Add New',
				'add_new_item'       => 'Add New Team Member',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Team Member',
				'new_item'           => 'New Team Member',
				'view'               => 'View',
				'view_item'          => 'View Team Member',
				'search_items'       => 'Search Team Member',
				'not_found'          => 'No Team Member found',
				'not_found_in_trash' => 'No Team Member found in Trash',
				'parent'             => 'Parent Team Member'
            ),
 
			'public'        => true,
			'menu_position' => 15,
			'supports'      => array( 'title', 'editor', 'comments', 'thumbnail'),
			'taxonomies'    => array( '' ),
			'menu_icon'     => 'dashicons-id',
			'has_archive'   => true
        )
    );
}

add_filter( 'rwmb_meta_boxes', 'team_member_meta_boxes' );
function team_member_meta_boxes( $meta_boxes ) {
    $prefix = 'team_member_';
    $meta_boxes[] = array(
        'title'      => 'Team Member',
        'post_types' => 'team_member',
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => 'Position',
                'id'    => $prefix . 'position',
                'type'  => 'text',
            ),
            array(
                'name'  => 'Email',
                'id'    => $prefix . 'email',
                'type'  => 'text',
            ),
            array(
                'name'  => 'Phone',
                'id'    => $prefix . 'phone',
                'type'  => 'text',
            ),
            array(
                'name'  => 'Website',
                'id'    => $prefix . 'website',
                'type'  => 'text',
            ),
            array(
                'name'  => 'Image',
                'id'    => $prefix . 'image',
                'type'  => 'single_image',
            ),
        )
    );

    return $meta_boxes;
}

add_shortcode('sc_team_member', 'show_team_member');
function show_team_member($atts) {
	$all = 1;
	if (!empty($atts['display'])) { //$atts['display'] must phone, email or website with comma (,) separator
		$all      = 0;
		$atts_arr = explode(',', $atts['display']);
	}

	$args = array(
		'post_type'      => 'team_member',
		'posts_per_page' => 10
    );              
	$posts = new WP_Query( $args );
	if ( $posts -> have_posts() ) {
	    while ( $posts -> have_posts() ) {
	        $posts->the_post();
			$tm_id       = get_the_ID();
			$img_post_id = get_post_meta($tm_id,'team_member_image',true);
	    	
	    	echo "<center>";the_title();echo "</center>";
	    	echo '<table style="border:2px red solid; text-align:center;">';
	    	echo '<tr><td><img src="'.get_the_guid($img_post_id).'" alt="" width="200" height="300"/></td></tr>';
	    	echo "<tr><td><b>".get_post_meta($tm_id,'team_member_position',true)."</b></td></tr>";
	  		if ($all||in_array('email', $atts_arr)) {
	  			echo "<tr><td>".get_post_meta($tm_id,'team_member_email',true)."</td></tr>";
	  		}
	  		if ($all||in_array('phone', $atts_arr)) {
	  			echo "<tr><td>".get_post_meta($tm_id,'team_member_phone',true)."</td></tr>";
	  		}
	  		if ($all||in_array('website', $atts_arr)) {
	  			echo "<tr><td>".get_post_meta($tm_id,'team_member_website',true)."</td></tr>";
	    	}
	    	echo "</table>";
	    }
	    
	} 
	wp_reset_query();
	
}


?>