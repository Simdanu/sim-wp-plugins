<?php
function optionsframework_option_name() {
	$optionsframework_settings = get_option('optionsframework');
	$optionsframework_settings['id'] = 'sim_theme_option';
	update_option('optionsframework', $optionsframework_settings);
}

function optionsframework_options() {
	$options = array();

	$options[] = array( "name" => "Third Theme Options", "type" => "heading" );

	$options['logo'] = array(
		"name" => "Logo",
		"desc" => "This creates a full size uploader that previews the image.",
		"id"   => "logo",
		"type" => "upload" );

	$options['maintenance_page'] = array(
		"name" => "Maintenance Page",
		"desc" => "Show maintenance mode page.",
		"id"   => "maintenance_page",
		"std"  => "0",
		"type" => "checkbox" );

	return $options;
}
?>