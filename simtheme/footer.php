	</div><!-- /.row -->

</div><!-- /.container -->

<div class="blog-masthead">
    <div class="container">
		<?php 
			wp_nav_menu( array( 
				'theme_location' => 'footer-menu', 
				'menu_class'     => 'nav nav-pills',
				'container'      => 'ul'
			) ); 
		?>
    </div>
</div>

<footer class="blog-footer">
    <?php if ( is_active_sidebar( 'footer-text' ) ) { dynamic_sidebar( 'footer-text' ); } ?>
	<?php @$first_theme = get_option('options_theme_first');  echo @$first_theme['footer_copyright']; ?>
</footer>
<?php wp_footer(); ?>
</body>
</html>