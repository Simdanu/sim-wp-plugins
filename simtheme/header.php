<!DOCTYPE html>
<html <?php language_attributes(); @$theme_option = get_option('sim_theme_option'); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   	<link rel="icon" href="<?php echo @$theme_option['logo']; ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="blog-masthead">
    <div class="container">
		<?php 
			wp_nav_menu( array( 
				'theme_location' => 'header-menu', 
				'menu_class'     => 'nav nav-pills',
				'container'      => 'ul'
			) ); 
		?>
    </div>
</div>

<div class="container">

    <div class="blog-header">
	    <h1 class="blog-title"><?php bloginfo( 'name' ); ?></h1>
	    <?php $description = get_bloginfo( 'description', 'display' ); ?>
	    <?php if($description) { ?><p class="lead blog-description"><?php echo $description ?></p><?php } ?>
		<?php echo @$theme_option['description']; ?>
	</div>

    <div class="row">