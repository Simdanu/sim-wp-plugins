<?php
 
add_action( 'wp_enqueue_scripts', 'bootstrapstarter_enqueue_styles' );
function bootstrapstarter_enqueue_styles() {
    wp_register_style('bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.min.css' );
    $dependencies = array('bootstrap');
    wp_enqueue_style( 'bootstrapstarter-style', get_stylesheet_uri(), $dependencies ); 
}

add_action( 'wp_enqueue_scripts', 'bootstrapstarter_enqueue_scripts' );
function bootstrapstarter_enqueue_scripts() {
    $dependencies = array('jquery');
    wp_enqueue_script('bootstrap', get_template_directory_uri().'/bootstrap/js/bootstrap.min.js', $dependencies, '3.3.7', true );
}

add_action( 'after_setup_theme', 'bootstrapstarter_wp_setup' );
function bootstrapstarter_wp_setup() {
    add_theme_support( 'title-tag' );
}

add_action( 'init', 'bootstrapstarter_register_menu' );
function bootstrapstarter_register_menu() {
    register_nav_menu('header-menu', __( 'Header Menu' ));
    register_nav_menu('footer-menu', __( 'Footer Menu' ));
}

add_action( 'widgets_init', 'bootstrapstarter_widgets_init' );
function bootstrapstarter_widgets_init() { 
    register_sidebar( array(
        'name'          => 'Footer - Copyright Text',
        'id'            => 'footer-text',
        'before_widget' => '<div class="footer-copyright-text">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name'          => 'Sidebar - Inset',
        'id'            => 'sidebar-1',
        'before_widget' => '<div class="sidebar-module sidebar-module-inset">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name'          => 'Sidebar - Default',
        'id'            => 'sidebar-2',
        'before_widget' => '<div class="sidebar-module">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>',
    ) );
}

add_action( 'pre_get_posts', 'posts_on_homepage' );
function posts_on_homepage( $query ) {
    $theme_option = get_option('sim_theme_option');
    $limit = (isset($theme_option['limit_post']))? $theme_option['limit_post'] : 5;
    if ( $query->is_home() && $query->is_main_query() ) {
        $query->set( 'posts_per_page', $limit );
    }
}

add_action('get_header', 'wp_maintenance_mode');
function wp_maintenance_mode() {
    $theme_option = get_option('sim_theme_option');
    $maintenance = (isset($theme_option['maintenance_page']))? $maintenance=$theme_option['maintenance_page'] : 0;
    if ((!current_user_can('edit_themes')||!is_user_logged_in()) && !empty($maintenance)) {
        wp_die("<h1>Under Maintenance</h1><br />Something ain't right, but we're working on it! Check back later.");
    }
}

?>