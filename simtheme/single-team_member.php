<?php get_header(); 

echo 'HELLO WORLD xxxx!!';
?>
<div class="col-sm-8 blog-main">
 
	<?php 
	if ( have_posts() ) { 
	 	while ( have_posts() ) : the_post();
	 ?>
		 	<div class="blog-post">
		 		<h2 class="blog-post-title"><?php the_title(); ?></h2>
		 			<p class="blog-post-meta"><?php the_date(); ?> by <?php the_author(); ?></p>
		 				<?php the_content(); 

						$tm_id       = get_the_ID();
						$img_post_id = get_post_meta($tm_id,'team_member_image',true);
				    	
				    	echo '<table style="text-align:center;">';
				    	echo '<tr><td><img src="'.get_the_guid($img_post_id).'" alt="" width="200" height="300"/></td></tr>';
				    	echo "<tr><td><b>".get_post_meta($tm_id,'team_member_position',true)."</b></td></tr>";
			  			echo "<tr><td>".get_post_meta($tm_id,'team_member_email',true)."</td></tr>";
			  			echo "<tr><td>".get_post_meta($tm_id,'team_member_phone',true)."</td></tr>";
			  			echo "<tr><td>".get_post_meta($tm_id,'team_member_website',true)."</td></tr>";
				    	echo "</table>";
		 				
		 				?>
		 	</div><!-- /.blog-post -->
	<?php
	 	endwhile;
	} 
	?>

	<nav>
		<ul class="pager">
			<li><?php next_posts_link('Previous'); ?></li>
			<li><?php previous_posts_link('Next'); ?></li>
		</ul>
	</nav>

</div><!-- /.blog-main -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>