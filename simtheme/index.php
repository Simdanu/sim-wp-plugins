<?php get_header(); ?>
<div class="col-sm-8 blog-main">
 
	<?php 
	if ( have_posts() ) { 
	 	while ( have_posts() ) : the_post();
	?>
		 	<div class="blog-post">
		 		<h2 class="blog-post-title"><?php the_title(); ?></h2>
		 			<p class="blog-post-meta"><?php the_date(); ?> by <?php the_author(); ?></p>
		 				<?php the_content(); ?>
		 	</div><!-- /.blog-post -->
	<?php
	 	endwhile;
	} 
	?>

	<nav>
		<ul class="pager">
			<li><?php next_posts_link('Previous'); ?></li>
			<li><?php previous_posts_link('Next'); ?></li>
		</ul>
	</nav>

</div><!-- /.blog-main -->

<?php 
$theme_option = get_option('sim_theme_option');
if (!isset($theme_option['display_sidebar'])||@$theme_option['display_sidebar']!=0) { get_sidebar(); } 
?>

<?php get_footer(); ?>